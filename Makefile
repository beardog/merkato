build:
	cd merkato; npm install; npm run build
	go build -o merkato-market merkato.go

clean:
	rm -rf merkato/dist
	rm -rf appserver