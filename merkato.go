package main

import (
	"io/fs"
	"embed"
	"log"
	"net/http"
	"os"
)
//go:embed merkato/dist/*
var site embed.FS

func getFileSystem() http.FileSystem {
    fsys, err := fs.Sub(site, "merkato/dist")
    if err != nil {
        log.Fatal(err)
    }
    return http.FS(fsys)
}

func main() {
	serverName := "Merkato"
	var bindAddress = os.Args[1]

	fs := http.FileServer(getFileSystem())

	http.Handle("/", fs)
	log.Println("Starting " + serverName + " web app on http://" + bindAddress + "...")
	err := http.ListenAndServe(bindAddress, fs)
	if err != nil {
		os.Stderr.WriteString("Error: " + err.Error() + "\n")
		os.Exit(1)
	}
}
