import fixURL  from './fixurl.js'

const onionrRPC = {
    queue_rpc: (session, method, id, params=[]) => {
        return fetch(fixURL.fixURL(session.onionrDaemonAddress + '/threaded_rpc'), {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                jsonrpc: '2.0',
                id: id,
                method: method,
                params: params
            })
        })
    },
    get_rpc_result: (session, id) => {
        return fetch(fixURL.fixURL(session.onionrDaemonAddress + '/get_rpc_result?id=' + id))
    },

    onionrRPC: (session, method, params=[]) => {

        return fetch(fixURL.fixURL(session.onionrDaemonAddress + '/rpc'), {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                jsonrpc: '2.0',
                id: '0',
                method: method,
                params: params
            })
        })
    }

}

export default onionrRPC