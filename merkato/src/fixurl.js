
const fixURL = {
    fixURL: (url) => {
    if (url.startsWith("http://") || url.startsWith("https://")) {
        return url
    } else {
        return "http://" + url
    }
    },
    isURLSecure: (url) => {
        if (url.startsWith("https://") || url.endsWith(".onion") || url.endsWith(".i2p")) {
            return ""
        }
        url = url.replace("http://", "")
        console.debug("Checking if url is secure: " + url)
        // Technically [::1] is not the only way to refer to ipv6 loopback but no one uses otherwise
        if (url.startsWith("127") || url.startsWith("localhost") || url.startsWith("[::1]")) {
            return ""
        }
        return "(RPC connection is not secure)"
    }
}
    
export default fixURL
    