import { sha3_256 } from 'js-sha3';

const _modulus = 2n ** 256n - 2n ** 32n * 351n + 1n;
const _little_fermat_expt = (_modulus * 2n - 1n) / 3n;
const _round_constants = Array.from({ length: 64 }, (_, i) => BigInt(i) ** 7n ^ 42n);



const onionrVDF = {
    powerMod: (base, exponent, modulus) =>  {
        let result = 1n;
        while (exponent > 0) {
        if (exponent % 2n === 1n) {
            result = (result * base) % modulus;
        }
        base = (base * base) % modulus;
        exponent = exponent / 2n;
        }
        return result;
    },

    forward_mimc: (input_data, steps) => {
        const encoder = new TextEncoder();
        const decoder = new TextDecoder();
        const inputBytes = encoder.encode(input_data);
        let inp = BigInt('0x' + Array.from(inputBytes, byte => byte.toString(16).padStart(2, '0')).join(''));

        for (let i = 1n; i < BigInt(steps); i++) {
            inp = (inp ** 3n + _round_constants[Number(i % BigInt(_round_constants.length))]) % _modulus;
        }

        const byteLength = Math.ceil(inp.toString(2).length / 8);
        const resultArray = new Uint8Array(byteLength);
        for (let i = 0; i < byteLength; i++) {
            resultArray[i] = Number((inp >> (8n * BigInt(i))) & 0xffn);
        }

        return decoder.decode(resultArray);
    },
    reverse_mimc: (input_data, steps) => {
        const encoder = new TextEncoder();
        const inputBytes = encoder.encode(input_data);
        let rtrace = BigInt('0x' + Array.from(inputBytes, byte => byte.toString(16).padStart(2, '0')).join(''));

        for (let i = BigInt(steps) - 1n; i > 0n; i--) {
            rtrace = onionrVDF.powerMod(rtrace - _round_constants[Number(i % BigInt(_round_constants.length))], _little_fermat_expt, _modulus);
        }


        const byteLength = Math.ceil(rtrace.toString(2).length / 8);
        const resultArray = new Uint8Array(byteLength);
        for (let i = 0; i < byteLength; i++) {
            resultArray[i] = Number((rtrace >> (8n * BigInt(i))) & 0xffn);
        }

        return resultArray.reverse()
    },
 
      
    uint8ToHex: (uint8arr) => { 
        let sum = 0;
        for (let i = 0; i < uint8arr.length; i++) {
          sum += uint8arr[i];
        }
        //console.debug(sum)

        let hex = "";
        for (let i = 0; i < uint8arr.length; i++) {
          let hexStr = uint8arr[i].toString(16);
          hex += (hexStr.length === 1 ? "0" + hexStr : hexStr);
        }
        return hex;
    },

    vdf_create: (data, rounds) => {
        const input_data = sha3_256.digest(data);
        //console.debug(input_data[5])
        console.debug(onionrVDF.reverse_mimc(input_data, rounds)[5])

        return onionrVDF.uint8ToHex(onionrVDF.reverse_mimc(input_data, rounds))
    },
    vdf_verify: (data, test_hash, rounds = DEFAULT_ROUNDS) => {
        const should_match = sha3_256.digest(data).slice(1);
        if (typeof test_hash === 'number') {
        test_hash = BigInt(test_hash).toString(16);
        }
        else {
        try {
            test_hash = BigInt('0x' + test_hash).toString(16);
        }
        catch {
            return false;
        }
        }
        const hash = BigInt('0x' + test_hash);
        return forward_mimc(hash, rounds) === should_match;
    }
}

export default onionrVDF