import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import { createI18n } from 'vue-i18n'
import 'bootstrap'

import './assets/main.css'
import 'bootstrap/scss/bootstrap.scss'
import { library } from '@fortawesome/fontawesome-svg-core'

import { faHatWizard } from '@fortawesome/free-solid-svg-icons'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { far } from '@fortawesome/free-regular-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'

library.add(fas, far, fab)
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
library.add(faHatWizard)

const app = createApp(App)

app.component('font-awesome-icon', FontAwesomeIcon)

const messages = {
    en: {
      message: {
        about: 'Merkato is a decentralized marketplace for Monero built ontop of Onionr.'
      }
    },
    es: {
      message: {
        about: 'hola'
      }
    }
  }

const i18n = createI18n({
    locale: 'en', // set locale
    fallbackLocale: 'en',
    messages

  })


app.use(router)
app.use(i18n)

app.mount('#app')
